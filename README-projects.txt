Title: GAIM-ify (not gamify) or Pidgin-ify the console

Description: I use GAIM (Pidgin) for a variety of accounts, but it
would be nice if I could access the console from GAIM, particularly
since I could use command-line clients directly from Pidgin. This
shouldn't be hard to do, since Pidgin already connects to IRC and it
seems you could just hack around to get this to work.

A telnet (or ncat) interface would suffice, and allow connections to
MOOs, MUSHes, etc (like tinyfugue aka tf), although few exist today.

My stack question on the subject: https://unix.stackexchange.com/questions/22791/can-i-use-gaim-pidgin-to-telnet-for-moos-mushes-muds-etc-like-tf/33518

====================================================================

Title: Better ad blocker: download ads but don't display them

Code URL: none

Description: Sites can detect ad blockers, so create a new one that
downloads their ad but never displays it to you, making it much harder
for them to tell you are blocking their ads.

====================================================================

Title: Jekyll-Wordpress fusion: locally edited blog w/ WP features

Code URL: none yet (but see "sub post_to_wp" in bclib.pl)

Description: allow local editing of a wordpress wiki as though it were
a jekyll wiki (ie, allow mirroring and easy post creations)

====================================================================

Title: Rosetta: convert code (especially mathematical formulas) between languages

Code URL: https://github.com/barrycarter/bcapps/blob/master/ROSETTA

Description: Convert Mathematica formulas into various other
programming languages

====================================================================

Title: Mt Fuji diamond: where can people see this phenomena?


Code URL: https://github.com/barrycarter/bcapps/blob/master/STACK/bc-fuji.m

Description: predict where people can see "Diamond Fuji", the sun
appearing as a "diamond ring" as it sets over Mount Fuji. An extended
version is https://github.com/barrycarter/bcapps/blob/master/DEM/
which attempts to determine the true horizon for any location using
digital elevation map (DEM) data. Diamond Fuji URLS/images:
https://www.tripadvisor.com/LocationPhotoDirectLink-g1104179-d1369080-i115862499-Lake_Yamanaka-Yamanakako_mura_Minamitsuru_gun_Yamanashi_Prefecture_Chub.html
https://www.fujiyama-navi.jp/en/entries/xZ0rQ
https://tripla.jp/cool-japan-diamond-fuji/
http://yamanakako.info/photo_diamond.php (page is in Japanese, and may
do what this project tries to do)
http://www2e.biglobe.ne.jp/%7Ewoody/mt97.jpg
http://www.gettyimages.fr/%C3%A9v%C3%A9nement/diamond-fuji-observed-156231577
https://www.garyjwolff.com/diamond-fuji-viewing-spots-dates-and-times-in-tokyo.html

====================================================================

Title: True sunrise and sunset times allowing for local topography

Code URL: https://github.com/barrycarter/bcapps/tree/master/DEM/

Description: A generalization of sorts of the project above, this
would use SRTM/DEM data to let people calculate where and when the sun
will set over non-flat/mountainous horizons

====================================================================

Title: Astronomical conjunctions

Code URL: https://github.com/barrycarter/bcapps/blob/master/ASTRO 

Showcase: http://search.astro.barrycarter.info/

Status: pretty much finished

=====================================================================

Title: OSM/Google Maps for stars, with a few more features

Code URL: https://github.com/barrycarter/bcapps/blob/master/JAVA

Notes: Sort of google maps for stars, probably already done to death though

=====================================================================

Title: Reprojected world maps from slippy tiles

Code URL: https://github.com/barrycarter/bcapps/blob/master/MAPS

Description: create images similar to the ones in the directory above,
but qgis and other GIS programs may already do this

=====================================================================

Title: Auto-populated wikis using meta-wiki pages

Code URL: https://github.com/barrycarter/bcapps/blob/master/METAWIKI/

Showcase: http://pbs3.referata.com/wiki/Main_Page

Description: create a semantic wiki from a single (or few) pages of data

=====================================================================

Subject: M.U.L.E. (game)
Code URL: https://github.com/barrycarter/bcapps/blob/master/YAMC
Showcase: none
Notes: a M.U.L.E. clone that can be played on an arbitrary sized map

Subject: Geolocation
Code URL: https://github.com/barrycarter/bcapps/blob/master/GEOLOCATION
Showcase: none yet, help me create one

Subject: Geography
Code URL: https://github.com/barrycarter/bcapps/blob/master/GEONAMES/
Showcase: http://albuquerque.weather.94y.info/ gives Albuquerque
current conditions for nearest weather station; in theory, any dotted
city notation in place of Albuquerque should work
TODO: Add sun/moon rise/set and dusk/dawn

Subject: Closed Captioning
Code URL: https://github.com/barrycarter/bcapps/blob/master/data/
Showcase: none really, attempt to collect DVD CC rips

Subject: Choose Your Own Adventure (books)
Code URL: https://github.com/barrycarter/bcapps/blob/master/CYOAGRAPH/
Showcase: none yet, help me create one

Subject: FreeDink (game)
Code URL: https://github.com/barrycarter/bcapps/blob/master/DINK
Showcase: none yet, help me create one

Subject: Misc
Code URL: https://github.com/barrycarter/bcapps/
Showcase: none, poke around and see if anything interests you

Subject: Entertainment
Code URL: https://github.com/barrycarter/bcapps/blob/master/VIDEO/
Showcase: https://www.youtube.com/watch?v=c5zBq0bPnW8 (but sucks)
Description: view multiple videos (eg, all episodes of a given series) very rapidly, multiple videos at a time (using tiling), no sound
TODO: improve showcase

Subject: Astronomy
Code URL: none yet
Description: Use the Hipparcos/HYG catalog (the largest one w/ distances) to create video of zooming around the universe; Celestia lets you view from other stars but not sure it allows you to see video of the travel

Subject: Astronomy
Code URL: https://github.com/barrycarter/bcapps/blob/master/ASTRO/bc-astro-formulas.m
Description: Closed form formulas for astronomy (useful when doing "meta astronomy" and not numerically precise astronomy)

Subject: Various
Code URL: NA
Description: Finish answering questions in STACK/README.status

Subject: Blogging
Code URL: WORDPRESS/README
Description: Command line Wordpress posting + actual things I want to post

TODO: write up these mini-ideas

  - fictious unix time zones

  - abusing dns text records to provide small chunks of info over UDP

  - automated character time measurement, scene measurement is another
  (in tv shows) [ie, conclusively figure out how much screen time each
  actor/character has w/o watching the movie/show]

  - GAIA2 star catalog in 3D

  - triangle dissection/shapes in general

  - game on real world map

  - most files in my git that are not answers to questions relate to
  some project


